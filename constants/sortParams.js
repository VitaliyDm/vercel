const sortParams = [{
    key: 'Location',
    field: 'location'
}, {
    key: 'Role',
    field: 'role'
}, {
    key: 'Department',
    field: 'department'
}, {
    key: 'Education',
    field: 'edu'
}, {
    key: 'Experience',
    field: 'exp'
}];

const sortType = {
    ASC: 'asc',
    DESC: 'desc',
}

const sortOrder = [sortType.ASC, sortType.DESC]

const sortFieldToJsonField = {
    'location': 'city',
    'role': 'job_title',
    'department': 'department',
    'edu': 'type',
    'exp': 'experience'
}

const experienceLvl = ['Internship', 'Junior', 'Intermediate', 'Senior'];

export default sortParams;
export {
    sortType,
  sortOrder,
  experienceLvl,
  sortFieldToJsonField
}
