import React, { useEffect } from "react";
import PropTypes from "prop-types";

import throttle from "../../utils/throttle";

import { INPUT_TIMEOUT } from "../../constants/common";

let handleSearch;

const SearchInput = ({ onSearch }) => {
  useEffect(() => {
    handleSearch = throttle((ev) => {
      onSearch(ev.target.value);
    }, INPUT_TIMEOUT);
  }, []);

  return (
    <div className="bg-white shadow p-4 flex">
      <span className="w-auto flex justify-end items-center text-gray-500 p-2">
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          stroke="currentColor"
          className="h-6 w-6"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            strokeWidth={2}
            d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"
          />
        </svg>
      </span>
      <input
        className="w-full rounded p-2"
        type="text"
        placeholder="Search for any job, title, keywords or company"
        onChange={handleSearch}
      />
    </div>
  );
};

SearchInput.propTypes = {
  onSearch: PropTypes.func,
};

SearchInput.defaultProps = {
  onSearch: () => {},
};

export default SearchInput;
