import React from "react";
import PropTypes from "prop-types";
import Block from "./Block";

const DEFAULT_MAX_ITEMS = 10;

const renderFilterItem = (filters) => {
  return filters.map((item) => (
    <span key={`${item.key}_${item.doc_count}`}>
      <p className="leading-8">
        {item.key}
        <span className="text-gray-400 ml-3">{item.doc_count}</span>
      </p>
    </span>
  ));
};

const FilterBlock = ({ title, filters, onShowMore }) => (
  <Block>
    <>
      <h2 className="uppercase font-medium leading-10">{title}</h2>
      {renderFilterItem(filters.slice(0, DEFAULT_MAX_ITEMS))}
      {filters.length > DEFAULT_MAX_ITEMS && (
        <button
          type="button"
          className="cursor-pointer text-blue-400"
          onClick={onShowMore}
        >
          Show more...
        </button>
      )}
    </>
  </Block>
);

FilterBlock.defaultProps = {
  title: "",
  filters: [],
  onShowMore: () => {},
};

FilterBlock.propTypes = {
  title: PropTypes.string,
  filters: PropTypes.arrayOf(PropTypes.object),
  onShowMore: PropTypes.func,
};

export default FilterBlock;
export { renderFilterItem };
