import React from "react";
import PropTypes from "prop-types";

import Block from "./Block";

import sortParams, { sortOrder, sortType } from "../../constants/sortParams";

const MOCK_ITEMS_COUNT = 20;

const renderSortParams = (sortBy, currentSort, handleClick) =>
  sortBy.map((item) => (
    <span className="flex gap-2" key={item.field}>
      <button
        type="button"
        key={item.key}
        className="ml-2"
        onClick={() => handleClick(item)}
      >
        {item.key}
      </button>
      {currentSort?.field === item.field &&
        (currentSort.sort === sortType.DESC ? (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="h-6 w-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M19 9l-7 7-7-7"
            />
          </svg>
        ) : (
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            stroke="currentColor"
            className="h-6 w-6"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth={2}
              d="M5 15l7-7 7 7"
            />
          </svg>
        ))}
    </span>
  ));

const renderMock = () =>
  new Array(MOCK_ITEMS_COUNT).fill(0).map((item, index) => (
    <div
      key={`mock_${index}`}
      className="animate-pulse flex space-x-4 h-8 mt-4"
    >
      <div className="rounded bg-gray-100 h-8 w-8" />
      <div className="h-4 mt-2 bg-gray-100 rounded w-3/4" />
    </div>
  ));

const renderFullJob = (job, openedJobs, toggleJob) => (
  <div key={job.id}>
    <div
      className="border-t-2 border-gray-100 p-2 pt-4 pb-4 md:flex justify-between md:cursor-pointer"
      onClick={() => toggleJob(job.id)}
    >
      <div>
        <h4 className="font-medium">{job.type}</h4>
        <p>
          {job.job_type} | ${job.salary_range[0]} - ${job.salary_range[1]} an
          hour | {job.city}
        </p>
      </div>
      <div className="leading-10">{job.created} weeks ago</div>
    </div>
    {openedJobs.includes(job.id) && (
      <div className="md:flex gap-10">
        <div className="flex flex-col gap-4 pl-2 pb-5">
          <div className="md:flex">
            <h4 className="font-medium flex-1">Department:</h4>
            <p className="flex-1">{job.department}</p>
          </div>
          <div className="md:flex">
            <h4 className="font-medium flex-1">Hours / shifts:</h4>
            <p className="flex-1">
              {job.hours[0]} / {job.work_schedule}
            </p>
          </div>
          <div className="md:flex">
            <h4 className="font-medium flex-1">Summary</h4>
            <p className="flex-1">{job.summary}</p>
          </div>
        </div>
        <div className="flex md:flex-col gap-2 flex-1 min-w-max mb-2 md:mb-0">
          <button
            type="button"
            className="bg-blue-400 text-white rounded-lg p-2 pl-4 pr-4"
          >
            Job details
          </button>
          <button
            type="button"
            className="rounded-lg border-blue-400 text-blue-400 border p-2 pl-4 pr-4"
          >
            Save job
          </button>
        </div>
      </div>
    )}
  </div>
);

const renderJobs = ({
  jobsToRender,
  openedJobs,
  toggleJob,
  handleOpenItem,
  openedItems,
}) => {
  if (jobsToRender.length) {
    return jobsToRender.map((item, index) => (
      <div key={`${item.name}_${index}`}>
        <div
          className="flex h-8 mt-4 hover:bg-gray-100 cursor-pointer pl-2"
          onClick={() => handleOpenItem(item.name)}
        >
          <div className="rounded bg-gray-200 h-8 w-8 text-white font-medium text-lg text-center leading-8">
            {item.name.slice(0, 2).toUpperCase()}
          </div>
          <div className="leading-6 ml-3 align-text-bottom md:leading-8 md:ml-4">
            {`${item.total_jobs_in_hospital} jobs for ${item.name}`}
          </div>
        </div>
        {openedItems.includes(item.name) && (
          <div className="mt-2">
            {item.items.map((job) => renderFullJob(job, openedJobs, toggleJob))}
          </div>
        )}
      </div>
    ));
  }

  return renderMock();
};

const MainBlock = ({ jobs, jobsCount, onSortChange }) => {
  const [openedItems, setOpenedItems] = React.useState([]);
  const [openedJobs, setOpenedJobs] = React.useState([]);
  const [currentSort, setSort] = React.useState({});

  const handleOpenItem = (itemName) => {
    if (openedItems.includes(itemName)) {
      setOpenedItems(openedItems.filter((item) => item !== itemName));
      return;
    }

    setOpenedItems([...openedItems, itemName]);
  };

  const toggleJob = (jobId) => {
    if (openedJobs.includes(jobId)) {
      setOpenedJobs(openedJobs.filter((item) => item !== jobId));
      return;
    }

    setOpenedJobs([...openedJobs, jobId]);
  };

  const handleChangeSort = (item) => {
    let newSort = {};
    if (item.field === currentSort.field) {
      if (sortOrder.indexOf(currentSort.sort) > 0) {
        newSort = {};
      } else {
        newSort = { field: item.field, sort: sortOrder[1] };
      }
    } else {
      newSort = {
        field: item.field,
        sort: sortOrder[0],
      };
    }

    setSort(newSort);
    onSortChange(newSort);
  };

  return (
    <Block>
      <>
        <div className="flex">
          <div className="flex-1">
            <p>
              <span className="mr-2 font-medium">{jobsCount}</span>
              job postings
            </p>
          </div>
          <div className="lg:flex-auto justify-end hidden md:flex">
            <span className="text-gray-400">Sort by</span>
            {renderSortParams(sortParams, currentSort, handleChangeSort)}
          </div>
        </div>
        <div className="mt-10">
          {renderJobs({
            jobsToRender: jobs,
            openedJobs,
            toggleJob,
            handleOpenItem,
            openedItems,
          })}
        </div>
      </>
    </Block>
  );
};

MainBlock.propTypes = {
  jobsCount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  jobs: PropTypes.arrayOf(PropTypes.object),
  onSortChange: PropTypes.func,
};

MainBlock.defaultProps = {
  jobsCount: "0",
  jobs: [],
  onSortChange: () => {},
};

export default MainBlock;
