import { useState, useEffect } from 'react';
import useSWR from "swr";

import Header from "../components/header/header";
import Container from "../components/container/container";
import Footer from "../components/footer/footer";
import SearchInput from "../components/input/searchInput";
import MainBlock from "../components/blocks/MainBlock";
import FilterBlock, { renderFilterItem } from "../components/blocks/FilterBlock";

// utils
import addCommasToNumber from "../utils/addCommasToNumber";

// constants
import sortParams, { sortOrder, sortType } from "../constants/sortParams";
import fetcher from "../utils/fetcher";
import Popup from "../components/popup/Popup";
import makeQueryParams from "../utils/makeQueryParams";

export const getStaticProps = async () => {
    const res = await fetch('http://localhost:3000/api/filters');
    let filters = await res.json();

    filters = Object.entries(filters).reduce((acc, [key, value]) => {
        acc[key] = value.map(item => ({ ...item, doc_count: addCommasToNumber(item.doc_count)}));

        return acc;
    }, {})

    return {
        props: {
            filters
        }
    }
}

const Index = ({ filters }) => {
    const [popupOpened, setPopupOpened] = useState(false);
    const [searchParam, setSearchParam] = useState('');
    const [sort, setSort] = useState({})

    const { data, mutate } = useSWR(
      makeQueryParams(`/api/jobs`, { ...sort, search: searchParam }),
      fetcher
    );

    const handleSearch = async (val) => {
        setSearchParam(val);
    }

    const togglePopup = () => {
        setPopupOpened(!popupOpened);
    }

    return (
    <div className="text-xs md:text-base">
        <Header/>
        <Container>
            <div className="grid grid-flow-row md:gap-7 md:pt-7 pb-9">
                <SearchInput onSearch={handleSearch}/>
                <div className="flex md:gap-7">
                    <div className="hidden md:flex max-w-xs md:gap-7 md:flex-col">
                        <FilterBlock
                          title="Job type"
                          filters={filters.job_type}
                        />
                        <FilterBlock
                          title="Department"
                          filters={filters.department}
                          onShowMore={togglePopup}
                        />
                        <FilterBlock
                          title="Work schedule"
                          filters={filters.work_schedule}
                        />
                        <FilterBlock
                          title="Experience"
                          filters={filters.experience}
                        />
                    </div>
                    <div className="md:flex-1">
                        <MainBlock
                            jobs={data?.jobs || []}
                            jobsCount={addCommasToNumber(
                              data?.jobs.reduce((acc, item) => item.total_jobs_in_hospital + acc, 0
                              ) || 0)}
                            onSortChange={setSort}
                        />
                    </div>
                </div>
            </div>
        </Container>
        <Footer/>
        <Popup opened={popupOpened} onClose={togglePopup} title="department">
            <div className="grid grid-flow-row-dense grid-cols-4 gap-3">
                {renderFilterItem(filters.department)}
            </div>
        </Popup>
    </div>);
}

export default Index
